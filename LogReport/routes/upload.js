var express = require('express');
var router = express.Router();
var multer = require("multer");
var upload = multer();
const fileExtension = require("file-extension");
const Zip = require("adm-zip");
var stringSearcher = require('string-search');
require("array.flatmap");

/* GET home page. */
router.get('/', function (req, res, next) {
  res.render('upload', { title: 'Report  from log' });
});

router.post('/file', upload.fields([{name: "files"}, {name:"choices"}]), function (req, res) {
  const choices = JSON.parse(req.body.choices);
  console.log(`Received ${choices.length} choices and ${req.files.files.length} files`);
  //console.debug("Choices", choices);
  //console.debug("Files", req.files);
  const logFiles = req.files.files.map(f => ({
    name: f.originalname,
    data: f.buffer
  })).flatMap(extract);
  console.log(`Expanded to ${logFiles.length} files`);

  const promises = logFiles.map(f => processFile(f, choices));

  Promise.all(promises).then(result => respond(res, result));
});

function processFile(file, choices) {
  console.log("Processing", file.name);
  const fileContent = file.data.toString("utf8");
  return new Promise((resolve, reject) => {
    const choicePromises = 
    choices.map(ch => {
      return stringSearcher.find(fileContent, ch.pattern)
              .then(result => Promise.resolve({
                query: ch.name,
                count: result.length
              }));
    });
    Promise.all(choicePromises).then(result => {
      resolve({
        fileName: file.name,
        results: result
      });
    })
  });
}

function isTextFile(file) {
  return ["txt", "log"].includes(fileExtension(file.name));
}

function isArchive(file) {
  return ["zip"].includes(fileExtension(file.name));
}

function unzip(file) {
  //console.log ("Unzipping...", file);
  const e = new Zip(file.data).getEntries().map(x => ({
    data: x.getData(),
    name: x.entryName
  }));
  //console.log("Unzip result", e);
  return e;
}

function extract(file) {
  if (isTextFile(file)) return [file];
  if (isArchive(file)) return unzip(file).flatMap(extract);
  return [];
}

function respond(res, val) {
  res.json(val);
  console.info('==========================> Handled post <=============================');
}

module.exports = router;
