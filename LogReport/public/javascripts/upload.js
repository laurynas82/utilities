function getSingleInput(element) {
    let result = {};
    for(let i = 0; i < element.childNodes.length; i++) {
        if (element.childNodes[i].className === "patternCheck") {
            result.selected = element.childNodes[i].checked;
        }
        if (element.childNodes[i].className === "patternName") {
            result.name = element.childNodes[i].value;
        }
        if (element.childNodes[i].className === "pattern") {
            result.pattern = element.childNodes[i].value;
        }
    }
    console.log(result);
    return result;
}

function getChoices() {
    const elems = Array.from(document.getElementById("choiceInput").children);
    return elems.map(getSingleInput).filter(x => x.selected);
}

function prepareDragAndDrop() {
    console.log("Initiated Drag & Drop");
    DragDrop("#dropHere", (files, pos, fileList, directories) => {
        console.log("Uploading", files);
        const formData = new FormData();
        formData.append("choices", JSON.stringify(getChoices()));
        files.forEach(f => formData.append("files", f));

        fetch("/upload/file", {
            method: "POST",
            body: formData
        })
        .then(resp => resp.json())
        .then(result => {
            console.log("Got result", result);
            displayResult(result);
        })
        .catch(err => {
            console.error(err);
        });
    }
    );
}

function displayResult(result) {
    const container = document.getElementById("result");
    const table = buildTable(result);
    let html = "<table>";
    for(let i = 0; i < table.length; i++) {
        html += "<tr>";
        for(let j = 0; j < table[i].length; j++) {
            html += i === 0 ? "<th>" : "<td>";
            html += table[i][j];
            html += i === 0 ? "</th>" : "</td>";
        }
        html += "</tr>";
    }
    html += "</table>";
    container.innerHTML = html;
}

function buildTable(result) {
    if (result.length === 0) return [];
    const table = [];
    let row = [""];
    result[0].results.forEach(r => row.push(r.query));
    table.push(row);
    result.forEach(r => {
        row = [r.fileName];
        r.results.forEach(r => row.push(r.count));
        table.push(row);
    });
    return table;
}