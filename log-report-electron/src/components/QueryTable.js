import React, { Component } from "react";
import Paper from "@material-ui/core/Paper";
import Button from "@material-ui/core/Button";
import AddIcon from "@material-ui/icons/Add";
import { withStyles } from '@material-ui/core/styles';
import { Table, TableHead, TableBody, TableRow, TableCell, TextField } from "@material-ui/core";
import DeleteIcon from "@material-ui/icons/Delete";
import IconButton from '@material-ui/core/IconButton';
import SearchIcon from "@material-ui/icons/Search";

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    button: {
        margin: theme.spacing.unit,
    },
    table: {
        minWidth: 700,
    },
});

class QueryTable extends Component {
    state = {
        nextId: 0,
        rows: []
    }

    handleAddRow() {
        this.setState({
            rows: [
                ...this.state.rows,
                {
                    id: this.state.nextId,
                    alias: "",
                    pattern: ""
                }
            ],
            nextId: this.state.nextId+1
        });
    }

    handleDelete(id) {
        this.setState({...this.state, rows: this.state.rows.filter(x => x.id !== id)});
    }

    handleEdit(id, fieldName, value) {
        const newRows = this.state.rows.map(r => r.id !== id ? r : {...r, [fieldName]: value});
        this.setState({...this.state, rows: newRows});
    }

    render() {
        const {classes} = this.props;
        return (
            <div style={{textAlign:"center"}}>
                <Paper className={classes.root}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell>Alias</TableCell>
                                <TableCell>Pattern</TableCell>
                                <TableCell></TableCell>
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.state.rows.map(r => (
                                <TableRow key={r.id}>
                                    <TableCell>
                                        <TextField
                                            placeholder="Enter an alias..."
                                            value={r.alias}
                                            fullWidth
                                            onChange={e => this.handleEdit(r.id, "alias", e.target.value)}
                                        />
                                    </TableCell>
                                    <TableCell>
                                        <TextField
                                            placeholder="Enter a pattern to search for..."
                                            value={r.pattern}
                                            fullWidth
                                            onChange={e => this.handleEdit(r.id, "pattern", e.target.value)}
                                        />
                                    </TableCell>
                                    <TableCell>
                                        <IconButton
                                            className={classes.button}
                                            aria-label="Delete"
                                            onClick={() => this.handleDelete(r.id)}
                                        >
                                            <DeleteIcon />
                                        </IconButton>
                                    </TableCell>
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Paper>
                <Button
                    variant="fab"
                    color="secondary"
                    aria-label="Add"
                    className={classes.button}
                    onClick={this.handleAddRow.bind(this)}
                >
                    <AddIcon />
                </Button>
                <Button
                    variant="fab"
                    color="primary"
                    aria-label="Add"
                    className={classes.button}
                    onClick={() => this.props.onSearch(this.state.rows)}
                >
                    <SearchIcon />
                </Button>
            </div>
        );
    }
}

export default withStyles(styles)(QueryTable);
