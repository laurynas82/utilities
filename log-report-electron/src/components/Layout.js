import React, { Component } from "react";
import Uploader from "./Uploader";
import QueryTable from "./QueryTable";
import ResultsTable from "./ResultsTable";
import { searchFile } from "../utils/fileSearcher";
import CircularProgress from "@material-ui/core/CircularProgress";

class Layout extends Component {
    state = {
        files: [],
        queries: []
    }

    runNewSearch(prevFiles, prevQueries) {
        this.setState({
            files: prevFiles.map(f => ({
                name: f.name,
                data: f.data,
                results: prevQueries.map(q => ({alias: q.alias, state: "LOADING"}))
            })),
            queries: prevQueries,
            isLoading: false
        }, () => {
            this.state.files.forEach(f => this.state.queries.forEach(q => {
                searchFile(f, q).then(r => {
                    const newFiles = this.state.files.map(fl => fl.name !== f.name ? fl : ({
                        ...fl,
                        results: fl.results.map(rl => rl.alias !== q.alias ? rl : r)
                    }));
                    //console.log(f, q, newFiles);
                    this.setState({...this.state, files: newFiles});
                });
            })) 
        });
    }

    handleUploadStart() {
        this.setState({...this.state, isLoading: true});
    }

    handleUploadEnd(files) {
        console.log("Uploaded", files);
        this.runNewSearch(files, this.state.queries);
    }

    handleSearch(queries) {
        console.log("Initiate search", queries);
        this.runNewSearch(this.state.files, queries);
    }

    render() {
        return (
            <div>
                <Uploader onUploadStart={this.handleUploadStart.bind(this)} onUploadEnd={this.handleUploadEnd.bind(this)}/>
                <QueryTable onSearch={this.handleSearch.bind(this)}/>
                { this.state.isLoading
                ? <CircularProgress />
                : <ResultsTable queries={this.state.queries} dataRows={this.state.files} />
                }
            </div>
        );
    }
}

export default Layout;
