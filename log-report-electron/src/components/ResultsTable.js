import React, { Component } from "react";
import Paper from "@material-ui/core/Paper";
import { withStyles } from '@material-ui/core/styles';
import { Table, TableHead, TableBody, TableRow, TableCell } from "@material-ui/core";
import CircularProgress from '@material-ui/core/CircularProgress';

const styles = theme => ({
    root: {
        width: '100%',
        marginTop: theme.spacing.unit * 3,
        overflowX: 'auto',
    },
    button: {
        margin: theme.spacing.unit,
    },
    table: {
        minWidth: 700,
    },
});

class ResultsTable extends Component {
    renderResult(result) {
        return result.state === "LOADING" ? <CircularProgress /> : result.result;
    }
    render() {
        const {classes} = this.props;
        return (
            <div style={{textAlign:"center"}}>
                <Paper className={classes.root}>
                    <Table className={classes.table}>
                        <TableHead>
                            <TableRow>
                                <TableCell></TableCell>
                                {this.props.queries.map((q,i) => (
                                    <TableCell key={i}>{q.alias}</TableCell>
                                ))}
                            </TableRow>
                        </TableHead>
                        <TableBody>
                            {this.props.dataRows.map((r,i) => (
                                <TableRow key={i}>
                                    <TableCell>{r.name}</TableCell>
                                    {r.results.map((res , j)=> (
                                        <TableCell key={j}>
                                            {this.renderResult(res)}
                                        </TableCell>
                                    ))}
                                </TableRow>
                            ))}
                        </TableBody>
                    </Table>
                </Paper>
            </div>
        );
    }
}

export default withStyles(styles)(ResultsTable);
