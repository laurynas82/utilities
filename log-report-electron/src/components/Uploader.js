import React, { Component } from "react";
import Dropzone from "react-dropzone";
import InsertDriveFile from "@material-ui/icons/InsertDriveFile";
import { withTheme } from '@material-ui/core/styles';
import { Typography, Paper } from "@material-ui/core";
import { flattenFiles } from "../utils/fileProcessor";

class Uploader extends Component {
    render() {
        const theme = this.props.theme;
        console.log(theme);
        const style = {
            padding: `${theme.spacing.unit}px ${theme.spacing.unit * 2}px`,
            color: theme.palette.text.primary,
            textAlign: "center",
            fontSize: "2em"
        }
        return (
            <Paper>
                <Dropzone
                    style={{backgroundColor: theme.palette.background.paper}}
                    onDrop={f => {
                        this.props.onUploadStart();
                        flattenFiles(f).then(r => this.props.onUploadEnd(r));
                    }}
                >
                    <Typography style={style}>
                        Drag files here <br/>
                        <InsertDriveFile fontSize="large" />
                    </Typography>
                </Dropzone>

            </Paper>
        );
    }
}

export default withTheme()(Uploader);
