import React, { Component } from "react";
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';
import { AppBar, Toolbar, Typography, IconButton } from "@material-ui/core";
import CssBaseline from '@material-ui/core/CssBaseline';
import Layout from "./components/Layout";
import SunOnIcon from "@material-ui/icons/WbSunny";
import SunOffIcon from "@material-ui/icons/WbSunnyOutlined";

class App extends Component {
    state = {
        isDarkThemeEnabled: true
    }
    render() {
        const theme = createMuiTheme({
            palette: {
                type: this.state.isDarkThemeEnabled ? "dark" : "light"
            },
        });
        return (
            <MuiThemeProvider theme={theme}>
                <CssBaseline />
                <AppBar position="static" color="default">
                    <Toolbar>
                        <Typography variant="title" color="inherit">
                            Log Report Generator
                        </Typography>
                        <IconButton aria-label="Delete" onClick={() => this.setState({isDarkThemeEnabled: !this.state.isDarkThemeEnabled})}>
                            { this.state.isDarkThemeEnabled
                            ? <SunOnIcon />
                            : <SunOffIcon />
                            }
                        </IconButton>
                    </Toolbar>
                </AppBar>
                <Layout />
            </MuiThemeProvider>
        );
    }
}

export default App;
