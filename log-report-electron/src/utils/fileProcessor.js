const Zip = require("adm-zip");
const fileExtension = require("file-extension");
const toBuffer = require("arraybuffer-to-buffer");
require("array.flatmap");

function isTextFile(file) {
    return ["txt", "log"].includes(fileExtension(file.name));
}

function isArchive(file) {
    return ["zip"].includes(fileExtension(file.name));
}

function unzip(file) {
    console.log ("Unzipping...", file);
    const e = new Zip(file.data).getEntries().map(x => ({
        data: x.getData(),
        name: x.entryName
    }));
    console.log("Unzip result", e);
    return e;
}

function extract(file) {
    if (isTextFile(file)) return [file];
    if (isArchive(file)) return unzip(file).flatMap(extract);
    return [];
}

const readFileAsync = file => new Promise((resolve, reject) => {
    const reader = new FileReader();
    reader.onload = () => {
        // console.log("Finished reading", file.name, reader.result);
        resolve({name: file.name, data: toBuffer(reader.result)});
    };
    reader.onabort = () => console.warn('file reading was aborted');
    reader.onerror = () => console.error('file reading has failed');

    reader.readAsArrayBuffer(file);
});

export const flattenFiles = files => {
    console.log("Flattening", files);
    return Promise.all(files.map(readFileAsync))
            .then(files => files.flatMap(extract));
}