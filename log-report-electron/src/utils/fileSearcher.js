const stringSearcher = require("string-search");

export const searchFile = (file, query) => {
    const fileContent = file.data.toString("utf8");
    return new Promise((resolve, reject) =>
        stringSearcher.find(fileContent, query.pattern)
                .then(result => resolve({
                    state: "LOADED",
                    result: result.length,
                    alias: query.alias
                }))
    );
}